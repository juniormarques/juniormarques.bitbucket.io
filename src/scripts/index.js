/* Máscaras ER */
function mascara(o, f) {
  v_obj = o;
  v_fun = f;
  setTimeout('execmascara()', 1);
}
function execmascara() {
  v_obj.value = v_fun(v_obj.value);
}
function mtel(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/^(\d{2})(\d)/g, '($1) $2'); //Coloca parênteses em volta dos dois primeiros dígitos
  v = v.replace(/(\d)(\d{4})$/, '$1-$2'); //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}
function id(el) {
  return document.getElementById(el);
}
window.onload = function () {
  id('phone').onkeyup = function () {
    mascara(this, mtel);
  };
};

let buttonShow = id('show');
let buttonClose = id('close');
let optionsNavbar = id('options');

buttonShow.onclick = function () {
  buttonClose.style.display = 'block';
  buttonShow.style.display = 'none';
  optionsNavbar.style.display = 'block';
  optionsNavbar.style.opacity = 1;
};

buttonClose.onclick = function () {
  buttonClose.style.display = 'none';
  buttonShow.style.display = 'block';
  optionsNavbar.style.display = 'none';
};

function visibleText1() {
  let text = id('text-duvidas1');
  let text2 = id('text-duvidas2');
  let text3 = id('text-duvidas3');
  let text4 = id('text-duvidas4');
  let text5 = id('text-duvidas5');

  let img1 = id('arrow');
  let img2 = id('arrow2');
  let img3 = id('arrow3');
  let img4 = id('arrow4');
  let img5 = id('arrow5');

  if (text.classList == 'duvidas-text1 visible') {
    text.classList.remove('visible');
    img1.src = './src/img/seta2.png';
  } else {
    if (text.classList == 'duvidas-text1') {
      text.classList.add('visible');
      text2.classList.remove('visible');
      text3.classList.remove('visible');
      text4.classList.remove('visible');
      text5.classList.remove('visible');

      img1.src = './src/img/seta.png';
      img2.src = './src/img/seta2.png';
      img3.src = './src/img/seta2.png';
      img4.src = './src/img/seta2.png';
      img5.src = './src/img/seta2.png';
    }
  }
}

function visibleText2() {
  let text = id('text-duvidas1');
  let text2 = id('text-duvidas2');
  let text3 = id('text-duvidas3');
  let text4 = id('text-duvidas4');
  let text5 = id('text-duvidas5');

  let img1 = id('arrow');
  let img2 = id('arrow2');
  let img3 = id('arrow3');
  let img4 = id('arrow4');
  let img5 = id('arrow5');

  if (text2.classList == 'duvidas-text2 visible') {
    text2.classList.remove('visible');
    img2.src = './src/img/seta2.png';
  } else {
    if (text2.classList == 'duvidas-text2') {
      text.classList.remove('visible');
      text2.classList.add('visible');
      text3.classList.remove('visible');
      text4.classList.remove('visible');
      text5.classList.remove('visible');

      img1.src = './src/img/seta2.png';
      img2.src = './src/img/seta.png';
      img3.src = './src/img/seta2.png';
      img4.src = './src/img/seta2.png';
      img5.src = './src/img/seta2.png';
    }
  }
}

function visibleText3() {
  let text = id('text-duvidas1');
  let text2 = id('text-duvidas2');
  let text3 = id('text-duvidas3');
  let text4 = id('text-duvidas4');
  let text5 = id('text-duvidas5');

  let img1 = id('arrow');
  let img2 = id('arrow2');
  let img3 = id('arrow3');
  let img4 = id('arrow4');
  let img5 = id('arrow5');

  if (text3.classList == 'duvidas-text3 visible') {
    text3.classList.remove('visible');
    img3.src = './src/img/seta2.png';
  } else {
    if (text3.classList == 'duvidas-text3') {
      text.classList.remove('visible');
      text2.classList.remove('visible');
      text3.classList.add('visible');
      text4.classList.remove('visible');
      text5.classList.remove('visible');

      img1.src = './src/img/seta2.png';
      img2.src = './src/img/seta2.png';
      img3.src = './src/img/seta.png';
      img4.src = './src/img/seta2.png';
      img5.src = './src/img/seta2.png';
    }
  }
}

function visibleText4() {
  let text = id('text-duvidas1');
  let text2 = id('text-duvidas2');
  let text3 = id('text-duvidas3');
  let text4 = id('text-duvidas4');
  let text5 = id('text-duvidas5');

  let img1 = id('arrow');
  let img2 = id('arrow2');
  let img3 = id('arrow3');
  let img4 = id('arrow4');
  let img5 = id('arrow5');

  if (text4.classList == 'duvidas-text4 visible') {
    text4.classList.remove('visible');
    img4.src = './src/img/seta2.png';
  } else {
    if (text4.classList == 'duvidas-text4') {
      text.classList.remove('visible');
      text2.classList.remove('visible');
      text3.classList.remove('visible');
      text4.classList.add('visible');
      text5.classList.remove('visible');

      img1.src = './src/img/seta2.png';
      img2.src = './src/img/seta2.png';
      img3.src = './src/img/seta2.png';
      img4.src = './src/img/seta.png';
      img5.src = './src/img/seta2.png';
    }
  }
}

function visibleText5() {
  let text = id('text-duvidas1');
  let text2 = id('text-duvidas2');
  let text3 = id('text-duvidas3');
  let text4 = id('text-duvidas4');
  let text5 = id('text-duvidas5');

  let img1 = id('arrow');
  let img2 = id('arrow2');
  let img3 = id('arrow3');
  let img4 = id('arrow4');
  let img5 = id('arrow5');

  if (text5.classList == 'duvidas-text5 visible') {
    text5.classList.remove('visible');
    img5.src = './src/img/seta2.png';
  } else {
    if (text5.classList == 'duvidas-text5') {
      text.classList.remove('visible');
      text2.classList.remove('visible');
      text3.classList.remove('visible');
      text4.classList.remove('visible');
      text5.classList.add('visible');

      img1.src = './src/img/seta2.png';
      img2.src = './src/img/seta2.png';
      img3.src = './src/img/seta2.png';
      img4.src = './src/img/seta2.png';
      img5.src = './src/img/seta.png';
    }
  }
}
